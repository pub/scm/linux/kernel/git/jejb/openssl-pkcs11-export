AC_INIT(openssl-pkcs11-export, 1.1.0, <openssl-tpm2-engine@groups.io>)
AM_INIT_AUTOMAKE([foreign 1.6])

AC_DISABLE_STATIC
AC_PROG_CC_STDC
AC_USE_SYSTEM_EXTENSIONS
AC_SYS_LARGEFILE
AC_PROG_LIBTOOL
AC_PROG_LN_S

PKG_CHECK_MODULES([CRYPTO], [libcrypto >= 3.0.0],
                  [ac_have_openssl3=1],
		  [PKG_CHECK_MODULES([CRYPTO], [libcrypto >= 1.0.2])])

# OAEP definitions are missing from earlier p11-kit
PKG_CHECK_MODULES([P11KIT], [p11-kit-1 >= 0.23.3])
AC_CHECK_PROGS([A2X], [a2x])
if test -z "$A2X"; then
   AC_MSG_ERROR([a2x from the asciidoc package must be installed to generate man pages])
fi
# test-sign is missing from gnutls < 3.4.4
AC_CHECK_PROG([P11TOOL], p11tool, [`p11tool --version|awk '/^p11tool /{print $2}'`])
if test  -z $P11TOOL; then
   AC_MSG_ERROR([p11tool from gnutls must be installed for make check])
fi
AX_COMPARE_VERSION([$P11TOOL],[ge],[3.4.4],[test_sign=yes])
AM_CONDITIONAL([TEST_SIGN], [test x$test_sign = "xyes"])
AM_COND_IF([TEST_SIGN],, [AC_MSG_WARN([p11tool is too old to run test signing])])


AM_CONDITIONAL(HAVE_OPENSSL3, test "$ac_have_openssl3" = "1")
if test -n "$ac_have_openssl3"; then
   DEPRECATION="-DOPENSSL_API_COMPAT=0x10100000L"
fi
AC_SUBST(DEPRECATION)

AC_ARG_WITH(
	[pkcs11-dir],
	[AS_HELP_STRING([--with-pkcs11-dir], [default PKCS11 module directory])],
	[pkcs11_dir="${withval}"],
	[PKG_CHECK_VAR([pkcs11_dir], [p11-kit-1], [p11_module_path])])

if test -z "$pkcs11_dir"; then
	AC_MSG_ERROR([no pkcs11 directory: either install p11-kit-devel or specify --with-pkcs11-dir])
fi
PKG_CHECK_VAR([pkcs11_configs], [p11-kit-1], [p11_module_configs])

LIBRARY_VERSION_MAJOR=`echo $PACKAGE_VERSION|cut -d. -f1`
AC_DEFINE_UNQUOTED(LIBRARY_VERSION_MAJOR, $LIBRARY_VERSION_MAJOR)
LIBRARY_VERSION_MINOR=`echo $PACKAGE_VERSION|cut -d. -f2`
AC_DEFINE_UNQUOTED(LIBRARY_VERSION_MINOR, $LIBRARY_VERSION_MINOR)
AC_CHECK_FUNCS(reallocarray)

AC_SUBST(pkcs11_dir)
AC_SUBST(pkcs11_configs)

AC_CONFIG_FILES([openssl-pkcs11.conf.5.txt])
AC_OUTPUT([Makefile tests/Makefile])

cat <<EOF

Library Version:		${LIBRARY_VERSION_MAJOR}.${LIBRARY_VERSION_MINOR}
pkcs11 module directory:	${pkcs11_dir}

EOF
