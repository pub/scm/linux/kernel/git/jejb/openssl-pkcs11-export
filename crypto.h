#ifndef _OPENSSL_CRYPTO_H
#define _OPENSSL_CRYPTO_H

#define ARRAY_SIZE(a) (sizeof (a) / sizeof ((a)[0]))

/* crypto.c functions */
void crypto_add_BN(int sec_num, const char *key, const BIGNUM *value,
		   EVP_MD_CTX *ctx);
void crypto_add_serial(int sec_num, EVP_MD_CTX *ctx);
EVP_PKEY_CTX *crypto_get_key(int sec_num);

/* crypto-rsa.c functions */
void crypto_rsa_populate(int sec_num, EVP_PKEY *pkey);
EVP_PKEY_CTX *crypto_rsa_add_padding(EVP_PKEY_CTX *ctx, CK_MECHANISM_PTR mech);
void crypto_rsa_fill_mechanism_list(int sec_num, unsigned long *mechs,
				    unsigned long *count);
int crypto_rsa_check_mechanism(int sec_num, CK_MECHANISM_TYPE mech,
			       CK_MECHANISM_INFO_PTR info);

/* crypto-ec.c functions */
void crypto_ec_populate(int sec_num, EVP_PKEY *pkey);
void crypto_ec_fill_mechanism_list(int sec_num, unsigned long *mechs,
				   unsigned long *count);
int crypto_ec_check_mechanism(int sec_num, CK_MECHANISM_TYPE mech,
			      CK_MECHANISM_INFO_PTR info);
int crypto_ec_sign(EVP_PKEY_CTX *ctx, void *data, unsigned long data_len,
		   void *sig, unsigned long *sig_len);

#endif
