#ifndef _OPENSSL_PKCS11_H
#define _OPENSSL_PKCS11_H

#include <p11-kit/pkcs11.h>

/* some versions of p11-kit have a missing OAEP settings */
#ifndef CKZ_DATA_SPECIFIED

/* PKCS#1 RSA OAEP Encoding Parameter Sources */
#define CKZ_DATA_SPECIFIED 0x00000001

#endif

#define ENV_CONFIG "OPENSSL_PKCS11_CONF"
#define CONFIG_FILE ".config/openssl-pkcs11/openssl-pkcs11.conf"
#define GLOBAL_SECTION "global"
#define GLOBAL_SECTION_NUM 0
#define INI_PUBLIC_KEY "public key"
#define INI_CERT "certificate"
#define INI_MANUFACTURERID "manufacturer id"
#define INI_MODEL "model"

#define CACHE_INT	(-1)
#define CACHE_PKEY	(-2)

#define BOOL_FOR_PUBLIC		(1<<0)
#define BOOL_FOR_PRIVATE	(1<<1)
#define BOOL_FOR_CERT		(1<<2)

/* crypto.c exported functions */
int crypto_load_public_key(int sec_num, const char *pub);
int crypto_load_cert(int sec_num, const char *cert);
int crypto_load_private_key(int sec_num, const unsigned char *pin, int pin_len);
void crypto_free_private_key(int sec_num);
void crypto_cache_free_pkey(void *pkey);
void *crypto_sign_init(int sec_num, CK_MECHANISM_PTR mech);
int crypto_sign(int sec_num, void *opdata, void *data, unsigned long data_len,
		void *sig, unsigned long *sig_len);
void *crypto_decrypt_init(int sec_num, CK_MECHANISM_PTR mech);
int crypto_decrypt(void *opdata, void *enc_data, unsigned long enc_len,
		   void *data, unsigned long *data_len);
void crypto_fill_mechanism_list(int sec_num, unsigned long *mechs,
				unsigned long *count);
int crypto_check_mechanism(int sec_num, CK_MECHANISM_TYPE mech,
			   CK_MECHANISM_INFO_PTR info);

/* ini.c exported functions */
int parse_ini_file(void);
void free_ini_file(void);

/* cache.c exported functions */
void cache_add(const char *section, const char *key, const char *value,
	       int len);
void cache_add_by_secnum(int sec_num, const char *key, const char *value,
			 int len);
const char *cache_get(const char *section, const char *key, int *len);
const char *cache_get_by_secnum(int sec_num, const char *key, int *len);
const char *cache_get_section(int sc);
int cache_get_sections(void);
void cache_load_crypto_keys(void);

/* used only for strings, so length doesn't matter */
static inline const char *cache_get_default(int sec_num,
					    const char *key,
					    const char *def)
{
	const char *val = cache_get_by_secnum(sec_num, key, 0);

	if (!val)
		val = def;

	return val;
}

#ifndef HAVE_REALLOCARRAY
#include <stdlib.h>
static inline void *reallocarray(void *ptr, size_t nmemb, size_t size)
{
	return realloc(ptr, nmemb * size);
}
#endif

#endif
