#!/bin/bash
set -x

P11TOOL="p11tool --provider ${srcdir}/../.libs/openssl-pkcs11-export.so"

##
# export the certificate and verify with openssl
##
rm -f tmp.crt
${P11TOOL} --export 'pkcs11:token=cert;object=cert;type=cert' > tmp.crt || exit 1
openssl verify -CAfile tmp.crt -check_ss_sig tmp.crt || exit 1
