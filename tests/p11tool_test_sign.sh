#!/bin/bash
set -x

P11TOOL="p11tool --provider ${srcdir}/../.libs/openssl-pkcs11-export.so"
CERTTOOL="certtool --provider ${srcdir}/../.libs/openssl-pkcs11-export.so"

GNUTLS_PIN=Passw0rd
export GNUTLS_PIN
##
# Move the certtool generate self signed test for RSA-PSS here because older
# gnutls fails
##
${CERTTOOL} --generate-self-signed --load-privkey 'pkcs11:token=key-pass;object=key-pass' --sign-params=RSA-PSS --template=tmp.tmpl > tmp.crt || exit 1
certtool --verify --infile tmp.crt --load-ca-cert tmp.crt || exit 1
##
# older gnutls also fails on all elliptic curve keys, so check them here
##
${CERTTOOL} --generate-self-signed --load-privkey 'pkcs11:token=key-p256;object=key-p256' --template=tmp.tmpl > tmp.crt || exit 1
certtool --verify --infile tmp.crt --load-ca-cert tmp.crt || exit 1

for f in "" "--sign-params=RSA-PSS"; do
    ${P11TOOL} --test-sign ${f} 'pkcs11:token=key-pass;object=key-pass' || exit 1
    ##
    # test signing to verify the public key we picked up from the
    # certificate is correct
    ##
    ${P11TOOL} --test-sign ${f} 'pkcs11:token=cert;object=cert' || exit 1
done
##
# Elliptic curve checks
##
${P11TOOL} --test-sign 'pkcs11:token=key-p256;object=key-p256' || exit 1
##
# gnutls cannot currently handle parametrised curves, only named ones
##
#${P11TOOL} --test-sign 'pkcs11:token=key-bp;object=key-bp' || exit 1
