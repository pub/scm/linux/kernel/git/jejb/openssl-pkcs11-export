check_signature() {
    diff -b tmp.txt recover.txt || exit 1
    rm -f recover.txt
}
echo "This is a message to sign" > tmp.txt
openssl rsautl -sign -engine pkcs11 -keyform engine -inkey 'pkcs11:token=key-nopass;object=key-nopass' -passin pass:random -in tmp.txt -out tmp.msg || exit 1
# verify recover
openssl rsautl -verify -pubin -inkey key-nopass.pub -in tmp.msg -out recover.txt || exit 1
check_signature
# check fail decrypt with wrong password
openssl rsautl -sign -engine pkcs11 -keyform engine -inkey 'pkcs11:token=key-pass;object=key-pass' -passin pass:random -in tmp.txt -out tmp.msg && exit 1
# check correct decryption with correct password
openssl rsautl -sign -engine pkcs11 -keyform engine -inkey 'pkcs11:token=key-pass;object=key-pass' -passin pass:Passw0rd -in tmp.txt -out tmp.msg || exit 1
# check recovery
openssl rsautl -verify -pubin -inkey key-pass.pub -in tmp.msg -out recover.txt || exit 1
check_signature
##
# PSS
##
for hash in sha1 sha224 sha256 sha384 sha512; do
    echo "PSS hash ${hash}"
    openssl ${hash} -out tmp.md -binary tmp.txt || exit 1
    openssl pkeyutl -sign -engine pkcs11 -keyform engine -inkey 'pkcs11:token=key-pass;object=key-pass' -pkeyopt rsa_padding_mode:pss -pkeyopt digest:${hash} -pkeyopt rsa_mgf1_md:${hash} -in tmp.md -out tmp.msg -passin pass:Passw0rd || exit 1
    ##
    # Would you believe openssl 1.0.2 will say the signature verified OK
    # but will then exit with a 1
    ##
    openssl pkeyutl -verify -inkey key-pass.pub -pubin -pkeyopt rsa_padding_mode:pss -pkeyopt digest:${hash} -pkeyopt rsa_mgf1_md:${hash} -in tmp.md -sigfile tmp.msg 2> /dev/null |grep 'Signature Verified Successfully'|| exit 1
done
##
# verify EC signature on a dummy certificate
##
for key in key-p256 key-bp; do
    openssl req -new -x509 -sha256 -subj '/CN=test/' -key "pkcs11:token=${key};object=${key}" -engine pkcs11 -keyform engine -out tmp.crt -passin pass:Passw0rd || exit 1
    openssl verify -CAfile tmp.crt -check_ss_sig tmp.crt || exit 1
done
