cat > ${OPENSSL_PKCS11_CONF} <<EOF
# test token config
manufacturer id = test-token
model = key
library description = set of tokens used for testing pkcs11 openssl

EOF
# generate two keys, one with a password and one without and create
# a config file for them
openssl genrsa 2048 > key-nopass.key || exit 1
openssl rsa -in key-nopass.key -pubout -out key-nopass.pub || exit 1
openssl genrsa -aes128 -passout pass:Passw0rd 2048 > key-pass.key || exit 1
openssl rsa -in key-pass.key -passin pass:Passw0rd -pubout -out key-pass.pub || exit 1
##
# create engine key equivalents by changing the guards
##
openssl pkcs8 -topk8 -in key-nopass.key -passout pass:Eng1ne -out tmp.key || exit 1
sed 's/ENCRYPTED PRIVATE KEY/TEST ENGINE PRIVATE KEY/' < tmp.key > key-engine.key
##
# EC KEY
##
# named curve
openssl genpkey -algorithm EC -pkeyopt ec_paramgen_curve:prime256v1 -aes-128-cbc -pass pass:Passw0rd -out key-p256.key || exit 1
openssl pkey -in key-p256.key -passin pass:Passw0rd -pubout -out key-p256.pub || exit 1
# parametrised curve
openssl genpkey -algorithm EC -pkeyopt ec_paramgen_curve:secp384r1 -pkeyopt ec_param_enc:explicit -aes-256-cfb -pass pass:Passw0rd -out key-bp.key || exit 1
openssl pkey -in key-bp.key -passin pass:Passw0rd -pubout -out key-bp.pub || exit 1
##
# Certificate (self signed)
##
openssl req -new -x509 -newkey rsa:2048 -subj "/CN=Test Key/" -keyout key-cert.key -out key-cert.crt -days 3650 -nodes -sha256 || exit 1
##
# now create a config file naming the keys
##
cat >> ${OPENSSL_PKCS11_CONF} <<EOF
[key-pass]
id = key1
public key = ${srcdir}/key-pass.pub
private key = ${srcdir}/key-pass.key

[key-nopass]
id = key2
private key = ${srcdir}/key-nopass.key
public key = ${srcdir}/key-nopass.pub

[key-engine]
id = key3
model = engine-key
manufacturer id = engine
engine = testengine
public key = ${srcdir}/key-pass.pub
private key = ${srcdir}/key-engine.key

[key-p256]
id = key-p256
public key = ${srcdir}/key-p256.pub
private key = ${srcdir}/key-p256.key

[key-bp]
id = key-bp
public key = ${srcdir}/key-bp.pub
private key = ${srcdir}/key-bp.key

[cert]
certificate = ${srcdir}/key-cert.crt
private key = ${srcdir}/key-cert.key

EOF
