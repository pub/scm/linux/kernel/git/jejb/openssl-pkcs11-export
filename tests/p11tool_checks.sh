#!/bin/bash
set -x

P11TOOL="p11tool --provider ${srcdir}/../.libs/openssl-pkcs11-export.so"
CERTTOOL="certtool --provider ${srcdir}/../.libs/openssl-pkcs11-export.so"

${P11TOOL} --list-mechanisms 'pkcs11:token=key-nopass'|awk '{print $2}' > tmp.txt
for mech in CKM_RSA_PKCS \
		CKM_RSA_X_509 \
		CKM_RSA_PKCS_PSS \
		CKM_RSA_PKCS_OAEP; do
    grep -q $mech tmp.txt || exit 1;
done

##
# Build a custom template file for a certificate
##
cat > tmp.tmpl <<EOF
cn = "Test Cert"
ca
EOF

export GNUTLS_PIN=Passw0rd
##
# Check RSA
##
${CERTTOOL} --generate-self-signed --load-privkey 'pkcs11:token=key-pass;object=key-pass' --template=tmp.tmpl > tmp.crt || exit 1
certtool --verify --infile tmp.crt --load-ca-cert tmp.crt || exit 1

##
# Make sure we can search the keyring
##
${P11TOOL} --list-all 'pkcs11:token=key-pass' > out.txt || exit 1
# should have one object
grep -q 'Object 0:' out.txt || exit 1
# and no second object
grep -q 'Object 1:' out.txt && exit 1
# now log into the key and prove we show two objects
${P11TOOL} --login --set-pin=Passw0rd --list-all 'pkcs11:token=key-pass' > out.txt || exit 1
# should have two objects
grep -q 'Object 0:' out.txt || exit 1
grep -q 'Object 1:' out.txt || exit 1
# and no third object
grep -q 'Object 2:' out.txt && exit 1
# and no second object
${P11TOOL} --login --list-all 'pkcs11:token=cert' > out.txt || exit 1
# cert should have 3 objects
grep -q 'Object 0:' out.txt || exit 1
grep -q 'Object 1:' out.txt || exit 1
grep -q 'Object 2:' out.txt || exit 1
# and no fourth object
grep -q 'Object 3:' out.txt && exit 1
# and no third object
##
# test operation with no config file (must always be last in the file)
##
OPENSSL_PKCS11_CONF=/random/nonexistent/config/file
# make sure p11tool returns an error
${P11TOOL} --list-all 2> stderr && exit 1
# and that the error says the provider failed to initialize
grep -q  'pkcs11_add_provider: PKCS #11 error' stderr || exit 1
