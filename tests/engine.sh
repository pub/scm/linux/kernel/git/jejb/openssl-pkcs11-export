##
# Small set of tests for engine keys.  The only real difference is the
# UI path for passwords, so only test that
##

check_signature() {
    diff -b tmp.txt recover.txt || exit 1
    rm -f recover.txt
}
echo "This is an engine message to sign" > tmp.txt
openssl rsautl -sign -engine pkcs11 -keyform engine -inkey 'pkcs11:manufacturer=engine;model=engine-key;token=key-engine;object=key-engine' -passin pass:Eng1ne -in tmp.txt -out tmp.msg || exit 1
# verify recover
openssl rsautl -verify -pubin -inkey key-nopass.pub -in tmp.msg -out recover.txt || exit 1
check_signature
