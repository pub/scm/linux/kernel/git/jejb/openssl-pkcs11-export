set -x
check_encryption() {
    diff -b tmp.txt recover.txt || exit 1
    rm -f recover.txt
}
echo "This is a message to encrypt" > tmp.txt
# simple encryption to public key using PKCS1.5 padding
openssl rsautl -encrypt -pubin -inkey key-nopass.pub -in tmp.txt -out tmp.msg || exit 1
# simple decrypt random password (token always requires 4+ digit pin)
openssl rsautl -decrypt -engine pkcs11 -keyform engine -inkey 'pkcs11:manufacturer=test-token;token=key-nopass;object=key-nopass' -passin pass:random -in tmp.msg -out recover.txt || exit 1
check_encryption
# encrypt to password requiring public key
openssl rsautl -encrypt -pubin -inkey key-pass.pub -in tmp.txt -out tmp.msg || exit 1
# check fail decrypt with wrong password
openssl rsautl -decrypt -engine pkcs11 -keyform engine -inkey 'pkcs11:token=key-pass;object=key-pass' -passin pass:random -in tmp.msg -out recover.txt && exit 1
# check correct decryption with correct password
openssl rsautl -decrypt -engine pkcs11 -keyform engine -inkey 'pkcs11:token=key-pass;object=key-pass' -passin pass:Passw0rd -in tmp.msg -out recover.txt || exit 1
check_encryption
##
# OAEP
##
for hash in sha1 sha224 sha256 sha384 sha512; do
    echo "OAEP hash ${hash}"
    openssl pkeyutl -encrypt -inkey key-pass.pub -pubin -pkeyopt rsa_padding_mode:oaep -pkeyopt rsa_oaep_md:${hash} -pkeyopt rsa_mgf1_md:${hash} -in tmp.txt -out tmp.msg || exit 1
    openssl pkeyutl -decrypt -engine pkcs11 -keyform engine -inkey 'pkcs11:manufacturer=test-token;model=key;token=key-pass;object=key-pass' -pkeyopt rsa_padding_mode:oaep -pkeyopt rsa_oaep_md:${hash} -pkeyopt rsa_mgf1_md:${hash} -in tmp.msg -out recover.txt -passin pass:Passw0rd || exit 1
    check_encryption
done
